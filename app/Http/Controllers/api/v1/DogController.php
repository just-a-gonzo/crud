<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\request\dog\Store;
use App\Http\Resources\v1\DogResource;
use App\Models\Dog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $dog = Dog::all();

        return  response()->json([
            'success' => $dog,
            200
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @return JsonResponse
     */
    public function store(Store $request) : JsonResponse
    {
        $dog = Dog::create($request->all());

        return  response()->json([
            'success' => $dog,
            200
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dog $dog) : DogResource
    {
        return new DogResource($dog);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dog $dog) : DogResource
    {
        $request->validate([
            'name' => 'required|string',
            'age' => 'required|integer',
        ]);

        $dog->update($request->all());

        return new DogResource($dog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dog $dog)
    {
        $dog->delete();

        return response()->json();
    }
}
