<?php


namespace App\Http\request\dog;


use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    public function rules() : array
    {
        return [
            'name' => 'required|string',
            'age' => 'required|integer',
        ];
    }

    public function authorize()
    {
        return true;
    }

}
